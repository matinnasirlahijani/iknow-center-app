import { StatusBar } from 'expo-status-bar';
import React , {Component,setState,useState,params,useEffect}from 'react';
import { render } from 'react-dom';
import { FlatList, StyleSheet, Text, View,ScrollView,ActivityIndicator,SafeAreaView} from 'react-native';
import { ListItem } from 'react-native-elements/dist/list/ListItem';
import {WebView} from 'react-native-webview';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ReviewScreen from './reviews';
import App from '../App';
import RenderHtml from 'react-native-render-html';
//import { Component } from 'react';
//const Stack = createNativeStackNavigator();
const ActivityIndicatorElement = () => {
  return (
    <View style={styles.activityIndicatorStyle}>
      <ActivityIndicator color="black" size="large" />
    </View>
  );
};
function webview ({route,navigation,Component}){
  
  


  const links = route.params;

  useEffect(() => {
    navigation.setOptions({ title: links.title.rendered })
  }, []);

  const [visible, setVisible] = useState(false);
    //const { data, isLoading } = ReviewScreen.state;
    return (
    
   
     <View style={styles.container}>
    <WebView ignoreSslError={true} nestedScrollEnabled={true} style = {styles.stage}
     
     source={{uri: links.link}} style={{marginTop: 0}}
     mediaPlaybackRequiresUserAction={true} javaScriptEnabled={true}
     domStorageEnabled={true}
     onLoadStart={() => setVisible(true)}
     onLoad={() => setVisible(false)}
          
         
      />
      {visible ? <ActivityIndicatorElement /> : null}
    
     
      </View>   
           
           
    );
    }
  
export default webview;

const styles = StyleSheet.create({
  stage: {
        
    flex:1,
    direction:'rtl',
    backgroundColor: '#EFEFF4',
    paddingBottom: 20,
  },
  // container: {
  //   flex: 1,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  // },
  activityIndicatorStyle: {
    flex: 1,
    position: 'absolute',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 'auto',
    marginBottom: 'auto',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
  },
    container: {
      backgroundColor: '#F5FCFF',
      flex: 1,
    },
    activityIndicatorStyle: {
      flex: 1,
      position: 'absolute',
      marginLeft: 'auto',
      marginRight: 'auto',
      marginTop: 'auto',
      marginBottom: 'auto',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      justifyContent: 'center',
    },
  });

 //  <View>
    //    <Text>
    //      {JSON.stringify(links.link)}
    //    </Text>

    //  </View>