import React, { Component ,setState,useState} from 'react';
import { render } from 'react-dom';
import { ActivityIndicator, FlatList, Text, View , ScrollView,StyleSheet,Image,Dimensions, TouchableOpacity} from 'react-native';
import { Cell, Section, TableView , Separator} from 'react-native-tableview-simple';
import {WebView} from 'react-native-webview';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
 import { NavigationContainer } from '@react-navigation/native';
 import { createNativeStackNavigator } from '@react-navigation/native-stack';
import webview from './webview';
import { scale } from 'react-native-size-matters';


class ReviewScreen extends Component{
  
    constructor(props) {
        super(props);
    
        this.state = {
          data: [],
          
          isLoading: true
        };
      }
    async getreviews() {
        try {
          const response = await fetch('https://iknowcenter.ir/wp-json/wp/v2/posts/?categories=2&&per_page=100');
          const json = await response.json();
          this.setState({ data: json });
        } catch (error) {
          console.log(error);
        } finally {
          this.setState({ isLoading: false });
        }
      }
      
    
      componentDidMount() {
        this.getreviews();
      }
        
      

      render() {
        const { navigation } = this.props;
        const { data, isLoading } = this.state;
        const {WebView} = this.props;
        const Stack = createNativeStackNavigator;
    return (
        // <ScrollView 
        // contentContainerStyle={styles.stage} 
        // nestedScrollEnabled={true}>  
        <View style = {styles.stage}> 
       <TableView appearance = 'dark'  />
        {isLoading ? <ActivityIndicator color = 'white' size = 'large' style = {styles.stage}/> : (
          <FlatList
            data={data}
            keyExtractor={({ id }, index) => id}
            renderItem={({ item }) => (

              <CellVariant cellStyle="Basic"  title = {item.title.rendered} onPress = 
              {() => navigation.navigate('web',item) } hideSeparator = {false}
             
              
             disableImageResize
            image = {<Image  style={{ borderRadius: 0 , height:100, width:100,backgroundColor: '#28282B'}} resizeMode= 'cover'
            source={{
              uri: item.better_featured_image.source_url,
            }}

            /> 
          
          } contentContainerStyle={{ paddingVertical: 0,paddingBottom:0 }} 
          
          
          />

            )} 

            ItemSeparatorComponent={({ highlighted }) => (
              <Separator isHidden={highlighted} />
            )}

          />
          
        )}

  
        
       
        </View> 
     // </ScrollView> 
  
  
      
    );

      }

      }


  


      // const CellVariant = (props) => (
      //   <Cell
      //     {...props}
      //     cellContentView={
      //       <View
      //         style={{ alignItems: 'center', flexDirection: 'row', flex: 1, paddingVertical: 10 }}
      //       >
      //         <Text
      //           allowFontScaling
      //           numberOfLines={1}
      //           style={{ flex: 1, fontSize: 20 }}
      //         >
      //           {props.title}
      //         </Text>
      //       </View>
      //     }
      //   />
      // );
export default ReviewScreen;
const { width , height} = Dimensions.get('window') ;
const windowWidth = Dimensions.get('window').width ;
const CellVariant = (props) => (
  <Cell
    {...props}
    cellContentView={
      <View
        style={{  paddingTop:0 , paddingBottom:0, height:100, width: '80%', backgroundColor: '#28282B'}}
      >
        <Text
          allowFontScaling = {true}
          numberOfLines={2}
          style={{ fontSize: scale(16) , fontWeight:'500', lineHeight:scale(30), color:'white', textAlign:'left', paddingRight:20 }}
        >
          {props.title}
        </Text>
      </View>
    }
  />
);



const styles = StyleSheet.create({
      stage: {
        flex:1,
        width:windowWidth,
        flexDirection:'row',
        //height:0,
        direction:'rtl',
        //backgroundColor: '#EFEFF4',
        backgroundColor: '#28282B',
         paddingTop: 3,
         paddingBottom: 0,
        paddingLeft:0
      },
      container: {
        flex: 1,
        paddingBottom:0,
         paddingTop:0,
        alignItems: 'center',
        justifyContent: 'center',
      },
      text: {
        fontSize: 40,
        fontWeight: 'bold',
      },
    });

      