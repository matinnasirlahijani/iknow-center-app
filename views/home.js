import React ,{useState,Component,setState,useEffect} from "react";
import { Button,View,StyleSheet,Text,Image , TouchableOpacity,navigation,Dimensions, ScrollView, FlatList,SafeAreaView,ActivityIndicator, ImageBackground} from "react-native";
import { FlatGrid } from 'react-native-super-grid';
import { SectionGrid } from 'react-native-super-grid';
import Carousel, { Pagination } from 'react-native-x2-carousel';
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import AutoScaleText from 'react-native-auto-scale-text'
//import { SwiperFlatList } from 'react-native-swiper-flatlist';
import ReviewScreen from "./reviews";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

function HomeScreen ({navigation,Component}) {
    
  
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getMovies = async () => {
     try {
      const response = await fetch('https://iknowcenter.ir/wp-json/wp/v2/posts/?categories=6&&per_page=10');
      const json = await response.json();
      setData(json);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getMovies();
  }, []);

  
  const [items] = useState([
    {title : ' آیتونز' , image :'https://www.freeiconspng.com/uploads/itunes-icon-9.png' ,goTo:'review'},
    {title : ' آیفون' , image :'https://images.macrumors.com/t/3q1wCX8q2ny5ej3QuD9ERZJsWDA=/800x0/smart/article-new/2019/10/iphone12-lineup-wide.jpg', goTo:'review'},
    {title : ' آیکلاد' , image :'https://www.freeiconspng.com/uploads/icloud-icon-12.png',goTo:'categories'},
    {title : 'بررسی تخصصی' , image :'https://www.freeiconspng.com/uploads/iphone-png-15.png',goTo:'review',}
  
  ]);
  

           

      return(
        
        //  <View style = {styles.containerS}>
   <ScrollView nestedScrollEnabled={true}>
    
       <View>
          {isLoading ? <ActivityIndicator color = 'white' size = 'large' style = {styles.container}/> : (
          <SwiperFlatList  autoplay
             style = {styles.containerS}
               autoplayDelay={3.5}
               index={3}
               autoplayLoop
               data={data}
               renderItem={({ item }) => 
       <TouchableOpacity onPress={() => navigation.navigate('web',item)}>

      <Image style = {styles.child} source = {{uri : item.better_featured_image.source_url} } resizeMode = 'cover'/>
               <Text style = {styles.Text} >
                  {item.title.rendered}
            </Text>
               
      </TouchableOpacity>
               
              
            }
            
          /> 
      )}
      </View> 



          <SectionGrid
         // style = {styles.itemContainer}
        sections={[
            {
              title: 'آموزش ها',
              data: items.slice(0, 6),
            }]}
        itemDimension={130}
        data={items}
        style={styles.gridView}
        // staticDimension={300}
        // fixed
        spacing={10}
        renderItem={({ item}) => (
          <View style={[styles.itemContainer]}>
           <TouchableOpacity onPress={() => navigation.navigate(item.goTo)}>
            <Image style={styles.itemimage} source = {{uri : item.image} } resizeMode = 'cover' >
            </Image>
            <Text style={styles.itemName}>{item.title}
            </Text>
            
            </TouchableOpacity> 
            
          </View>
          
        )}
        renderSectionHeader={({ section }) => (
            <Text style={styles.sectionHeader}>{section.title}</Text>)}
            
            
      />
      
        
     </ScrollView>
     
      // </View>
    );
      
}
export default HomeScreen;
const { width , height} = Dimensions.get('window');
const styles = StyleSheet.create({
    container :{
        flex :1,
        height :100,
       // direction:'rtl'
    },
    gridView: {
        flex: 1,
        marginTop: 10,
        flexDirection:'column',
        //paddingLeft:100
       // paddingLeft:90,
       // marginRight:0,
        //marginBottom:200,
      },
      itemContainer: {
        paddingLeft:'15%',
        paddingTop:10,
        justifyContent:'flex-end',
        //width: 100,
        //height: 150,
        //justifyContent: 'flex-end',
      },
      itemName: {
        lineHeight:0,
        width: scale(100),
        fontWeight:100,
        //height:100,
        top:scale(10),
        borderRadius:50,
        fontSize: 20,
        paddingLeft:scale(15),
        color: '#FFFFFF',
        fontWeight: '600',
      },
      
      itemimage: {
        //flex:1,
        borderRadius: 100 ,
        //alignItems:'center',
        //paddingLeft:15,
        height:100, 
        width:100,
      },
      sectionHeader: {
        flex:1,
        flexDirection:'row-reverse',
        paddingLeft:scale(250),
        fontSize: 15,
        fontWeight: '600',
        backgroundColor: '#28282B',
        color: 'white',
        padding: 10,
      },
      item: {
        flex:1,
        width:500,
        height: 250,
        //alignItems: 'center',
        //justifyContent: 'center',
        backgroundColor: '#dbf3fa',
      },
      carouselImage:{
        flex:1,
        width:500,
        height:0,
      },
      carouselText:{
        height:20,
        fontSize: 15,
        fontWeight: '600',
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: '#636e72',
        color: 'grey',
        padding: 0,
      },
      stage: {
        width:100,
        flex:1,
        height:0,
        direction:'rtl',
        //backgroundColor: '#EFEFF4',
         paddingTop: 3,
         paddingBottom: 0,
      },
      child: { 
        width : width ,
        borderRightColor:'red',
        //width:scale(100),
        height: height * 0.33,
         //justifyContent: 'center',
         borderRadius:30,
         //position: 'relative',
          //left: 0,
           //top: 0,
         //borderRadius:70,
         },
        Text: { 
          //width:scale(100),
         //height: verticalScale(100),
         // padding: moderateScale(5),
          width: scale(300),
          fontSize: 20,
        paddingLeft: 50,
        paddingTop: -100,
        textAlign: 'center',
        color: 'white',
     },
         containerS: { 
        borderRadius:30,
        flex: 1,
        //backgroundColor: 'white' ,
      
      },
      first:{
        width:100
      }
});