import React, { Component } from 'react';
import { render } from 'react-dom';
import { ActivityIndicator, FlatList, Text, View , ScrollView,StyleSheet,Image,Dimensions,TouchableOpacity} from 'react-native';
//import { Cell, Section, TableView } from 'react-native-tableview-simple';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Icon } from 'react-native-elements';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { NavigationContainer , DarkTheme,useTheme,} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ReviewScreen from './views/reviews';
import webView from './views/webview';
import CatScreen from './views/categories';
import datas from './data';
import getData from './getData';
import webview from './views/webview';
import HomeScreen from './views/home';

// const CustomSectionHeader = () => (
//   <View>
//     <Text>Custom header!</Text>
//   </View>
// );
const Tab = createBottomTabNavigator();


function MyTabBar({ state, descriptors, navigation }) {
  return (
    <View style={{ flexDirection: 'row' ,}}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };
      

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{ flex: 1 }}
          >
            <Text style={{ color: isFocused ? '#673ab7' : '#222' }}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}


function MyTabs() {
  
  return (
   
    
      <Tab.Navigator   initialRouteName="Home"
      screenOptions={{
        tabBarActiveTintColor: '#FFFFFF', backBehavior: 'order',
      }}>

        
       
 
      <Tab.Screen name="categories" component={CatScreen} 
       
       options={{
         tabBarLabel:  'دسنه بندی ها', title:'دسته بندی ها',
         tabBarIcon: ({ color, size }) => (
           <MaterialCommunityIcons name="book" color={color} size={size} />
         ),}}/>

         <Tab.Screen name="review" component={ReviewScreen} 
        
        options={{
         title : "بررسی تخصصی",
          tabBarLabel: 'بررسی تخصصی',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="apple" color={color} size={size}  />
          ),
        }}/>

         <Tab.Screen 
         name = "Home" component = {HomeScreen}
         options = {{ title:'آموزش ها',
           tabBarLabel : 'آموزش ها', tabBarIcon : ({color , size}) =>(
            <MaterialCommunityIcons name = "home" color = {color} size = {size} />
           ),
         }}
         
         />
{



/* <Tab.Screen name="web" component={webView} 
       
       options={{
         tabBarLabel:  'وب',
         tabBarIcon: ({ color, size }) => (
           <MaterialCommunityIcons name="web" color={color} size={size} />
         ),
       }}
       /> */}
       
      </Tab.Navigator>
      // </NavigationContainer>
     

    
  );
}
const MyTheme = {
  dark: true,
  colors: {
    primary: 'rgb(211, 211, 211)',
    background: '#28282B',
    card: 'rgb(40, 40, 43)',
    text: 'rgb(211, 211, 211)',
    border: 'rgb(40, 40, 43)',
    notification: 'rgb(40, 40, 43)',
  },
};
export default function App() {
  const Stack = createNativeStackNavigator();
  return (
    <NavigationContainer independent={true} theme = {MyTheme}> 
    {/* <MyTabs /> */}
    <Stack.Navigator>
      <Stack.Screen 
       name = "tabs" component = {MyTabs} options={{ headerShown: false }}
      />
      <Stack.Screen 
      name = "web" component = {webView} title = "site"
      />
    </Stack.Navigator>
  </NavigationContainer>
  
  );
}

// export default function ReactNavigationBottomTabs() {
//     return(
//       <Tab.Navigator
//      tabBarOptions={
//        {
//       // Default Color is blue you can change it by following props
//       // activeTintColor: '#ff4757',
//       // inactiveTintColor: '#ff6b81',
//       // Default Background Color is white you can change it by following props
//       // activeBackgroundColor: '#ced6e0',
//       // inactiveBackgroundColor: '#ced6e0',
//        }
//   }
//         >
//         <Tab.Screen
//          name='دسته بندی ها'
//          component={category}
//          options={{
//           tabBarIcon: ({ color, size }) => (
//             <Icon name='home' color={color} size={size} />
//         ),
//     }}
//              />
//     </Tab.Navigator>
//     );

// }